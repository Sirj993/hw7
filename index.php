<?php
$tours = [
    [
        'title' => 'Villa Mirko',
        'country' => 'Черногория, Бечичи',
        'date' => '10.06.2021',
        'price' => ['327 usd', '9174 uah'],

    ],
    [
        'title' => 'Ring Beach',
        'country' => 'Турция, Кемер',
        'date' => '11.05.2021',
        'price' => ['258 usd', '7218 uah'],
    ],
    [
        'title' => 'Tu Casa Gelidonya',
        'country' => 'Турция, Кемер',
        'date' => '09.05.2021',
        'price' => ['206 usd', '5762 uah'],
    ],
    [
        'title' => 'Side Premium',
        'country' => 'Турция, Сиде',
        'date' => '12.05.2021',
        'price' => ['284 usd', '7942 uah'],
    ],
    [
        'title' => 'Lujo Bodrum',
        'country' => 'Турция, Бодрум',
        'date' => '15.05.2021',
        'price' => ['1830 usd', '51232 uah'],
    ],
    [
        'title' => 'Q Premium',
        'country' => 'Турция, Аланья',
        'date' => '09.05.2021',
        'price' => ['361 usd', '10104 uah'],
    ],
    [
        'title' => 'Rehana Sharm',
        'country' => 'Египет, Шарм-эль-Шейх',
        'date' => '09.05.2021',
        'price' => ['275 usd', '7700 uah'],
    ],
    [
        'title' => 'Xperience Sea Breeze',
        'country' => 'Египет, Шарм-эль-Шейх',
        'date' => '06.05.2021',
        'price' => ['275 usd', '7700 uah'],
    ],
    [
        'title' => 'Sunrise Montemare',
        'country' => 'Египет, Шарм-эль-Шейх',
        'date' => '07.05.2021',
        'price' => ['957 usd', '26808 uah'],
    ],
    [
        'title' => 'Рospitable Georgia',
        'country' => 'Грузия, Батуми',
        'date' => '25.06.2021',
        'price' => ['385 usd', '10772 uah'],
    ],
]

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>
<div class="container">
        <div class="row">
            <div class="col-12">
            <table class="table table-striped">
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Страна</th>
                    <th>Дата</th>
                    <th>Цена</th>
                </tr>
                <?php foreach($tours as $key => $tour):?>
                    <tr>
                        <td><?=++$key?></td>
                        <td><?=$tour['title']?></td>
                        <td><?=$tour['country']?></td>
                        <td><?=$tour['date']?></td>
                        <td>
                            <?php foreach($tour['price'] as $priceMain):?>
                                <li>
                                    <?= $priceMain?>
                                </li>
                            <?php endforeach?>
                        </td>
                    </tr>
                <?php endforeach;?>
            </table>
            </div>
        </div>
    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>